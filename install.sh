#!/bin/bash

rebootRequired=0

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

source opt/epubli/office-alert/etc/config.sh

if [[ $1 == 'configure' ]]; then
    # install neccessary third-party apps (@TODO: composer)
    apt-get update && apt-get upgrade
    apt --assume-yes install apache2 mpg123 vorbis-tools mediainfo
    apt-get --assume-yes install alsa-base alsa-utils alsa-mixer
    apt-get --assume-yes install mplayer
    apt-get --assume-yes install wiringpi
    apt-get --assume-yes install id3v2
    apt-get --assume-yes install bc ntpdate ntp
    apt-get --assume-yes install nmap
    apt-get --assume-yes install php
    # apt-get install software-properties-common
    # apt-get install python3.6

    # CREATE ROOT DIRECTORY
    mkdir -p $INSTALL_ROOT

    # clean up old files
    read -p "Wipe out old installation directory (This will also delete all sound files!)?" INPUT
    if [[ $INPUT =~ ^[Yy]$ ]]
    then
        rm -R $INSTALL_DIR
        echo "Notice: Removed old installation directory and subfolders."
    fi
fi

# copy files from installation bundle to opt-folder
cp -a $PWD/opt/epubli/office-alert $INSTALL_ROOT

# create symlink in user home directory
ln -s $INSTALL_DIR ~
ln -s $PATH_SOUNDS ~

# enable password-free sudo usage
REGX='#includedir.*/etc/sudoers\.d'
FILE="/etc/sudoers"

if grep -q $REGX $FILE ;then
   echo "Notice: sudoers.d-subdirectory was already enabled."
else
   echo "#includedir /etc/sudoers.d" >> $FILE
   echo "Notice: sudoers.d-subdirectory added to sudoers includedir."
fi

# activate "nolirc"-setting
REGX='nolirc=yes'
FILE="/etc/mplayer/mplayer.conf"

if grep -q $REGX $FILE ;then
   echo "Notice: nolirc-directive has already been set"
else
   echo "nolirc=yes" >> $FILE
   echo "Notice: nolirc-directive is now active"
fi

# enable text2speech module support
REGX='snd_bcm2835'
FILE="/etc/modules"

if grep -q $REGX $FILE ;then
   echo "Notice: module 'snd_bcm2835' has already been activated"
else
   echo "snd_bcm2835" >> $FILE
   echo "Notice: module 'snd_bcm2835' has been activated"
fi


# copy sudoers for www-access
cp etc/sudoers.d/* /etc/sudoers.d

if [[ $1 == 'configure' ]]; then


    echo "Setting up raspberry device configuration"
    . raspi-config nonint

    hostname=$(get_hostname)
    read -p "Please specify a hostname. [default: $hostname]" INPUT
    INPUT=${INPUT:-$hostname}
    do_hostname "$INPUT"

    echo "expanding filesystem"
    do_expand_rootfs 0

    echo "activating SSH support"
    do_ssh 0

    locale-gen de_DE

    echo "setting up locale settings"
    update-locale LANG="de_DE" LC_ALL="de_DE"

    read -p "Do you want to configure a static ip-address for your device?" INPUT
    if [[ $INPUT =~ ^[Yy]$ ]]
    then

        # setting up network settings skeleton
        echo "Copying default network interface settings."
        cp etc/network/interfaces /etc/network

        # setting up final network settings according to user input
        echo "Please provide the network information for this device to be set up with."
        for i in "${!USER_SETTINGS[@]}"
        do
          read -p "Please enter an IP-address for $i [default: ${USER_SETTINGS[$i]}]:" INPUT
          USER_SETTINGS[$i]=${INPUT:-${USER_SETTINGS[$i]}}
          TEMP=${USER_SETTINGS[$i]}
          sed -i "s@<%USER_VAR_$i%>@$TEMP@g" /etc/network/interfaces
        done
    fi

    read -p "Do you want to configure the WIFI-settings for your device?" INPUT
    if [[ $INPUT =~ ^[Yy]$ ]]
    then
        read -p "Please specify the Network Name (SSID):" SSID
        read -p "Please specify an Encryption Method:" ENCRYPTION
        read -p "Please specify a password:" PASSWORD
        NETID=$(wpa_cli add_network | tail -n 1)
        wpa_cli set_network $NETID ssid \"$SSID\"
        case $ENCRYPTION in
        'WPA')
            wpa_cli set_network $NETID key_mgmt WPA-PSK
            wpa_cli set_network $NETID psk \"$PASSWORD\"
            ;;
        'WEP')
            wpa_cli set_network $NETID wep_key0 $PASSWORD
            wpa_cli set_network $NETID wep_key1 $PASSWORD
            wpa_cli set_network $NETID wep_key2 $PASSWORD
            wpa_cli set_network $NETID wep_key3 $PASSWORD
            ;;
        *)
            ;;
        esac
        wpa_cli enable_network $NETID
        wpa_cli save_config
    fi
    rebootRequired=1
#    service networking restart

fi

# configure and launch apache webserver
echo "Setting up vhost and enable cgi for Apache2"
cp etc/apache2/sites-available/001-epubli-office-alert.conf /etc/apache2/sites-available
a2dissite 000-default.conf
a2ensite 001-epubli-office-alert.conf
a2enmod cgid
a2enmod rewrite
systemctl daemon-reload
echo "Restarting Apache2 webserver"
service apache2 restart

# set owner for sound files
chown www-data:www-data $PATH_UPLOAD

# set permissions for executable www
chown -R www-data:www-data $PATH_CGI $PATH_BASH
chmod 0755 -R $PATH_CGI $PATH_BASH
echo "Updated permissions for executables (755)"

chown -R www-data:www-data $PATH_CACHE $PATH_DATA $PATH_SOUNDS
chmod 0777 -R $PATH_CACHE $PATH_DATA $PATH_SOUNDS
echo "Enable write permissions to cache directory"

# set default audio parameters

alsactl init
amixer cset numid=3 1
amixer sset 'PCM' 100%
alsactl store

# installation finished
echo "Installation complete"

if [ $rebootRequired == 1 ]; then
    read -p "A reboot is required to have the changes come into effect. Reboot now?" INPUT
    if [[ $INPUT =~ ^[Yy]$ ]]
    then
        reboot
        echo "Notice: Removed old installation directory and subfolders."
    fi
fi