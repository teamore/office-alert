Epubli Office Alert

The Epubli Office Alert serves the purpose of providing a (multi-channel) 
platform for monitoring/messaging and triggering alert devices.

In order to install the Office Alert, please execute

* chmod 0777 install.sh
* ./install.sh

with root privileges.