#!/usr/bin/env bash

cd ~
git clone https://github.com/abrasive/shairport.git
sudo apt-get install avahi-utils libssl-dev libao-dev libpulse-dev libasound2-dev
sudo apt-get install autoconf automake avahi-daemon build-essential git libasound2-dev libavahi-client-dev libconfig-dev libdaemon-dev libpopt-dev libssl-dev libtool xmltoman
cd shairport
./configure
make
sudo make install
cd scripts/debian/
sudo cp -r * /etc/
sudo adduser --system --disabled-login --ingroup audio shairport

echo -e "USER=shairport\nGROUP=audio\nNICE=0\nLOGFILE=/var/log/shairport.log\nERRFILE=/var/log/shairport.err\nPIDFILE=/var/run/shairport.pid\nAP_NAME=$('hostname')" >> /etc/default/shairport

sudo update-rc.d shairport defaults
sudo service shairport start
