#!/usr/bin/env bash

# language
export LC_ALL=de_DE.UTF-8

# master='192.168.178.52'

APP_NAME="Office Alert"
APP_VERSION="1.0"

INSTALL_ROOT="/opt/epubli"
INSTALL_DIR="$INSTALL_ROOT/office-alert"

# define paths to 3rd party binaries
PATH_SENDSCRIPT=$INSTALL_DIR"/bin/send"
PATH_CODESEND=$INSTALL_DIR"/bin/codesend"
PATH_PLAYMP3="/usr/bin/mpg123"
PATH_PLAYOGG="/usr/bin/ogg123"
PATH_PLAYWAV="/usr/bin/mplayer"
PATH_AMIXER="/usr/bin/amixer"
PATH_MPLAYER="/usr/bin/mplayer"

# application persistent storage paths
PATH_CACHE=$INSTALL_DIR"/var/cache"
PATH_DATA=$INSTALL_DIR"/var/data"
PATH_WWW=$INSTALL_DIR"/var/www"

# application bash script resources
PATH_BASH=$INSTALL_DIR"/src/bash"
# application cgi script resources
PATH_CGI=$INSTALL_DIR"/bin/cgi"

URI_ROOT="/"
URI_CGI=$URI_ROOT'alert/'

# path for uploaded media files
PATH_UPLOAD=$INSTALL_DIR"/var/upload"

# path for uploaded sound files
PATH_SOUNDS=$INSTALL_DIR"/var/upload/sounds"

# define key value storage resource files
FILE_CACHE=$PATH_CACHE"/sounds.meta.cache"
FILE_DATA=$PATH_DATA"/sounds.meta.data"
FILE_QUOTA=$PATH_DATA"/sounds.meta.quota"
FILE_NETWORK=$PATH_DATA"/network.meta.data"

# define default parameters
VERBOSE=0
SYSTEMCODE=11111
DURATION=0
VOLUME="100%"
DEVICE=6

# DEFINE DEFAULT SETTINGS:
declare -A USER_SETTINGS
USER_SETTINGS[HOST]=192.168.42.208
USER_SETTINGS[HOST_WIFI]=192.168.42.209
USER_SETTINGS[GATEWAY]=192.168.42.1
USER_SETTINGS[NAMESERVER]=192.168.42.1
USER_SETTINGS[NETMASK]=255.255.255.0

declare -A HOSTS
HOSTS["192.168.42.210"]="Marketing,MASTER"
HOSTS["192.168.42.208"]="IT,SLAVE"
HOSTS["192.168.42.212"]="Support,SLAVE"
HOSTS["192.168.42.214"]="Konfi,SLAVE"
HOSTS["192.168.42.218"]="Foyer,SLAVE"

# DEFINE QUOTAS:

# defines how long a sound is blocked from repetitive playback after being triggered
QUOTA_REPETITION=600
# defines how many sounds may be played back during the given period altogether
# i.e. to allow no more than 20 sounds per hour per device (QUOTA_TRIGGER_LIMIT=20 QUOTA_TRIGGER_PERIOD=3600):
QUOTA_TRIGGER_LIMIT=30
QUOTA_TRIGGER_PERIOD=3600
# ignore all quotas (e.g. for system notification sounds) residing underneath the designated folder
QUOTA_IGNORE_PATH="Epubli"

# synchronization buffer to level out latencies / netlags in seconds
# (results in a delay when playing back sounds synchronously)
SYNC_BUFFER="1.5"
