<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 07.11.17
 * Time: 14:09
 */
header('Content-Type: application/json');

$soundDir = '/opt/epubli/office-alert/var/upload/sounds';
$metaDir = '/opt/epubli/office-alert/var/data';
$categoryDirectories = scandir($soundDir);

$categories = array();

foreach($categoryDirectories as $categoryDir){
    $category = new stdClass;
    $category->name = $categoryDir;
    $category->directorySize = getDirSize($soundDir.'/'.$categoryDir);
    $category->itemCount = getDirFileCount($soundDir.'/'.$categoryDir);

    $fileNames = scandir($soundDir.'/'.$categoryDir);
    $files = array();
    if($categoryDir == '.' || $categoryDir == '..')
        continue;

    foreach($filesNames as $fileName){
        $file = new stdClass;
        $file->name = $fileName;
        $soundMetaData = getFileMetaData($fileName);
        $file->lastPlayed = $soundMetaData->lastPlayed;
        $file->timesPlayed = $soundMetaData->timesPlayed;
        array_push($files, $file);
    }

    $files = $file;
    $category->files = $files;
    array_push($categories, $category);
}
echo json_encode($categories);
function getDirSize($directory) {
    $size = 0;
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
        $size += $file->getSize();
    }
    return $size;
}


function getDirFileCount($directory) {
    $count= 0;
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
        $count++;
    }
    return $count;
}

function getFileMetaData($fileName){
    $metaDir = 'opt/epubli/office-alert/var/data';
    $metaDataObject = new stdClass;
    $metaDataObject->lastPlayed = 0;
    $metaDataObject->timesPlayed = 0;
    $metaFile = fopen($metaDir. '/'. $fileName);
    if ($metaFile) {
        while (($line = fgets($metaFile)) !== false) {
            if(strpos($line, $fileName)){
                $metaData = explode(',', explode(':', $line));
                $metaDataObject->timesPlayed = $metaData[0];
                $metaDataObject->lastPlayed = $metaData[1];
            }
        }
        fclose($metaFile);
    } else {
        // error opening the file.
    }
    return $metaDataObject;
}