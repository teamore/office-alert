import {soundFile} from "./soundFile";

export class Directory {
    dirName: string;
    soundFiles : soundFile [];
    dirSize : string;
    filesCount: number;
    constructor(_dirName, _dirSize, _count){
        this.dirName = _dirName;
        this.dirSize = _dirSize;
        this.filesCount = _count;
    }

    getSoundFiles(): soundFile[]{
        return this.soundFiles;
    }

    addSoundFiles(soundFile): void {
        this.soundFiles.push(soundFile);
    }
    setSoundFiles(soundFiles): void {
        this.soundFiles = soundFiles;
    }
}