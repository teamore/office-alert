/**
 * Created by teamore on 05.09.17.
 */
function parseData(data, source, delimiter, fields) {
    for (var i = 0; i < source.length; ++i) {
        var buf = source[i].split(delimiter[0]);
        var bufrow = source[i].substr(buf[0].length + 1);
        var val = bufrow.split(delimiter[1]);
        if (bufrow) {
            if (!(buf[0] in data)) data[buf[0]] = [];
            for (var ii = 0; ii < fields.length; ++ii) {
                data[buf[0]][fields[ii]] = val[ii];
            }
        }
    }
    return data;
}
function getSetting(parameter, defaultValue) {
    return (Document.SETTINGS[parameter] ? Document.SETTINGS[parameter] : defaultValue);
}
function initList(update) {
    <!-- listing.innerHTML = Document.data; -->
    start_time = new Date();
    block = Document.data.split(/\n{2,}/g);
    list = block[0].split("\n");
    options = block[1].split("\n");
    dirs = block[2].split("\n");
    cache = block[3].split("\n");
    data = block[4] ? block[4].split("\n") : [];

    Document.SETTINGS = [];
    for (var i = 0; i < options.length; i++) {
        val = options[i].split("\t");
        if (!update) {
            console.log(val[0] + "=" + val[1]);
        }
        Document.SETTINGS[val[0]] = val[1];
    }
    META = [];
    parseData(META, data, [":", ","], ["plays", "lastPlayed"]);
    parseData(META, dirs, [" ", " "], ["size", "numfiles"]);
    parseData(META, cache, [":", ","], ["type", "bitrate", "duration", "created", "lastAccess", "size"]);
    uncached = [];
    locked = [];
    quoted = [];
    IP = Document.SETTINGS["HOST_IP"];
    HOSTNAME = Document.SETTINGS["HOSTNAME"];

    if (typeof IP !== 'undefined' && IP) {
        channel = getSetting("HOSTS[" + IP + "]");

        if (typeof channel !== 'undefined') {
            channel = channel.split(",");
            document.title = channel[0] + " " + channel[1] + " | Epubli Office Alert";
            document.getElementById("channelTitle").innerHTML = channel[0];
            document.getElementById("channelType").innerHTML = channel[1];
        } else {
            document.title = HOSTNAME + " | Epubli Office Alert";
            document.getElementById("channelTitle").innerHTML = HOSTNAME;
            document.getElementById("channelType").innerHTML = IP;
        }
    }

    var files = 0,
        files_search = 0,
        dirs = 0,
        bytes = 0,
        bytes_search = 0;

    prev = document.getElementById("directories");
    for (var i = 0; i < list.length; ++i) {
        val = list[i].split(" ");
        var cached = true;
        if (!META[val[0]]) {
            uncached.push(val[0]);
            META[val[0]] = [];
            cached = false;
        }
        META[val[0]]['path'] = val[0];
        var filesize = (META[val[0]]['size'] > 0 ? META[val[0]]['size'] / 1000 : 0);
        bytes += filesize;
        if (update) {
            el = document.getElementById('file-' + val[0]);
            if (el) {
                hide = el.classList ? el.classList.contains('hide') : false;
                match = el.classList ? el.classList.contains('search-match') : false;
                files += is_file = createRow(el, META[val[0]]);
                dirs += !is_file;
                files_search += match;
                if (is_file && hide && !match) {
                    el.classList.add('hide');
                }
                if (match) {
                    bytes_search += filesize;
                }
            }
        } else {
            el = AddBefore(prev);
            is_file = createRow(el, META[val[0]]);
            files += is_file;
            dirs += !is_file;
            if (is_file) {
                el.classList.add('hide');
            }
        }
        if (el) {
            el.classList.add(cached ? 'cache' : 'no-cache');
        }
    }
    if (uncached.length > 0) {
        console.log("NOTICE: uncached files detected:");
        console.log(uncached);
        triggerCacheUpdate(uncached);
    }
    // show meta and summary info
    if (document.getElementById("diskUsage") !== null)
        document.getElementById("diskUsage").innerHTML = readableBytes((bytes_search ? bytes_search : bytes) * 1000);
    if (document.getElementById("filesAmount") !== null)
        document.getElementById("filesAmount").innerHTML = (files_search > 0 ? files_search : files);
    if (document.getElementById("dirsAmount") !== null)
        document.getElementById("dirsAmount").innerHTML = (document.querySelectorAll('tr.directory').length - document.querySelectorAll('.no-matches').length);

    if (document.getElementById("quotaInfo") !== null)
        document.getElementById("quotaInfo").innerHTML =
            "Repetition quota: <strong>" + getSetting("QUOTA_REPETITION") + "</strong> seconds | Trigger limit: <strong>" + quoted.length + "</strong> / <strong>" + getSetting("QUOTA_TRIGGER_LIMIT") + "</strong> (" + getSetting("QUOTA_TRIGGER_PERIOD") + " seconds)"
    end_time = new Date();
    if (!update) {
        console.log('directory listing created in ' + (Math.floor(end_time - start_time) / 1000) + ' seconds');
    }
}
function createRow(element, metadata) {
    var path = metadata['path'];
    var pathname = metadata['path'] ? metadata['path'].replace(/\/.*?$/, '').replace(/[\\ ]/, '-').replace(/\..+$/, '') : '';
    if (path.match(/.+\.[a-zA-Z0-9]{2,3}/g)) {
        // we are dealing with a file
        metadata['type'] = path.substr(path.lastIndexOf('.') + 1);
        element.classList.add('file', metadata['type'], "path-" + pathname);
        if (metadata['lastPlayed']) {
            var diff = dateDiff(metadata['lastPlayed']);
            if (diff > 0) {
                if (diff < parseInt(getSetting('QUOTA_REPETITION'))) {
                    locked.push({
                        path: metadata['path'],
                        seconds: diff
                    });
                    el.classList.add('quota-violation-repetition');
                } else {
                    el.classList.remove('quota-violation-repetition');
                }
                if (diff < parseInt(getSetting('QUOTA_TRIGGER_PERIOD'))) {
                    quoted.push({
                        path: metadata['path'],
                        seconds: diff
                    });
                }
            }
        }
        metadata['date'] = metadata['lastPlayed'] ? metadata['lastPlayed'] : (metadata['lastAccess'] ? metadata['lastAccess'] : (metadata['created'] ? metadata['created'] : ''));
        metadata['basename'] = metadata['path'] ? metadata['path'].split("/").pop().replace(/\..+$/, '') : '';
        metadata['id'] = "file-" + metadata['path'];
        if (metadata['basename']) {
            element.innerHTML = showFile(metadata);
            el.setAttribute('data-parent-dir', pathname);
            element.id = metadata['id'];
        }
        return 1;
    } else {
        // we are dealing with a directory
        element.innerHTML = showDir(metadata);
        element.classList.add('directory');
        metadata['id'] = "path-" + metadata['path'];
        element.id = metadata['id'];
    }

    return 0;
}
function triggerCacheUpdate(uncachedFiles) {
    var client = new XMLHttpRequest();
    uncachedFiles = uncachedFiles.splice(0, 10);
    client.open('GET', '/alert/cache-update.sh?' + uncachedFiles.join("+"));
    client.onload = function (responseData) {
        console.log("cache updater response:");
        console.log(this.responseText);
    };
    client.send();
}
function AddBefore(target) {
    var newElement = document.createElement('tr');
    target.parentNode.insertBefore(newElement, target);
    return newElement;
}

function AddAfter(target) {
    var newElement = document.createElement('tr');

    target.parentNode.insertBefore(newElement, target.nextSibling);
    return newElement;
}
function speak(text) {
    var client = new XMLHttpRequest();
    var append = getSelectedOptions(document.getElementById('networkChannel'));
    var url = '/alert/trigger.sh?speech=' + text + (append ? '&channel=' + append : '');
    console.log('text2speech call: ' + url);
    client.open('GET', url);
    client.onload = function (responseData) {
        console.log("text2speech response:");
        console.log(this.responseText);
    };
    client.send();
}
function getSelectedOptions(el, fn) {
    opts = [];
    // loop through options in select list
    for (var i = 0, len = el.options.length; i < len; i++) {
        opt = el.options[i];

        // check if selected
        if (opt.selected) {
            // add to array of option elements to return from this function
            opts.push(opt.value);

            // invoke optional callback function if provided
            if (fn) {
                fn(opt);
            }
        }
    }
    return opts;
}

function triggerSound(file) {
    var client = new XMLHttpRequest();
    var append = getSelectedOptions(document.getElementById('networkChannel'));
    var pitch = document.getElementById("pitch").value;
    var url = '/alert/trigger.sh?sound=' + file + (append ? '&channel=' + append : '');
    if (pitch) {
        url = url + "&pitch=" + pitch;
    }

    client.open('GET', url);
    client.onload = function (responseData) {
        console.log("file playback response:");
        console.log(this.responseText);
        updateList(true);
    };
    client.send();
}

function convertTime(t, format) {
    if (Number(t) == t) {
        var y = Math.floor(t / 60 / 60 / 24 / 365);
        var m = Math.floor(t / 60 / 60 / 24 / 30);
        var w = Math.floor(t / 60 / 60 / 24 / 7);
        var d = Math.floor(t / 60 / 60 / 24);
        var h = Math.floor(t / 60 / 60 % 24);
        var min = Math.floor(t / 60 % 60);
        var s = Math.floor(t % 60);

        ms = t % 1;
        tstr = '';
        nbsp = '\u00A0';

        if (format === 'short') {
            if (y > 0) tstr = y + nbsp + "year" + (y > 1 ? 's' : '');
            else if (m > 0) tstr = m + nbsp + "month" + (m > 1 ? 's' : '');
            else if (w > 0) tstr = w + nbsp + "week" + (w > 1 ? 's' : '');
            else if (d > 0) tstr = d + nbsp + "day" + (d > 1 ? 's' : '');
            else if (h > 0) tstr = h + nbsp + "hour" + (h > 1 ? 's' : '');
            else if (min > 0) tstr = min + nbsp + "minute" + (min > 1 ? 's' : '');
            else if (s > 0) tstr = s + nbsp + "second" + (s > 1 ? 's' : '');

        } else {
            if (d > 0) tstr = d + ":" + h + ":" + min + ":" + s;
            else if (h > 0) tstr = h + ":" + min + ":" + s;
            else if (min > 0) tstr = min + ":" + s;
            else if (s > 0) tstr = s;
        }
        if (format === 'full') {
            tstr += "." + ms;
        }
        return tstr;
    }
}

function dateDiff(start, end) {
    if (undefined === end) {
        end = new Date();
    } else if (Number(end) == end) {
        end = new Date(end * 1000);
    } else {
        // cut milliseconds and timezone appendix ( e.g "2017-08-31 15:45:54"><".148332101 +0200" )
        end = end.length >= 19 ? end.substr(0, 19) : end;
        end = new Date(end.replace(/-/g, '/'));
    }
    if (undefined === start) {
        return 0;
    } else if (Number(start) == start) {
        start = new Date(start * 1000);
    } else {
        // cut milliseconds and timezone appendix ( e.g "2017-08-31 15:45:54"><".148332101 +0200" )
        start = start.length >= 19 ? start.substr(0, 19) : start;
        start = new Date(start.replace(/-/g, '/'));
    }
    diff = Math.floor((end - start) / 1000);
    return diff;
}

function readableBytes(bytes) {
    if (bytes >= 1024 * 1024 * 1024) {
        return (Math.floor(bytes / 1024 / 1024 / 1024 * 10) / 10) + "G";
    } else if (bytes > 1024 * 1024) {
        return (Math.floor(bytes / 1024 / 1024 * 10) / 10) + "M";
    } else if (bytes > 1024) {
        return (Math.floor(bytes / 1024)) + "K";
    } else {
        return bytes;
    }

}
function toggleFiles($class) {
    if (document.selectedDir && ($class !== document.selectedDir)) {
        document.getElementById(document.selectedDir).classList.remove('active');
        cusid_ele = document.getElementsByClassName(document.selectedDir);
        for (i = 0; i < cusid_ele.length; ++i) {
            item = cusid_ele[i];
            item.classList.add('hide');
        }
    }
    document.getElementById($class).classList.toggle('active');
    var cusid_ele = document.getElementsByClassName($class);
    for (var i = 0; i < cusid_ele.length; ++i) {
        var item = cusid_ele[i];
        item.classList.toggle('hide');
    }
    document.selectedDir = $class;
}

function showDir($meta) {
    toggleJs = "onclick='toggleFiles(\"path-" + $meta['path'] + "\");'";
    date = dateDiff($meta['lastAccess']);
    htmltext = "<tr> \
            <td class='type' " + toggleJs + ">   \
                <div class='preview' id='" + $meta['path'] + "'>" + readableBytes($meta['size']) + "</div> \
            </td> \
            <td class='title' " + toggleJs + "> \
               <span class='title'>" + $meta['path'] + "</span> \
            </td> \
            <td class='date' " + toggleJs + " colspan=2><span class='date'>" + (date ? date : '') + "</span></td> \
            <td class='duration' " + toggleJs + "><span class='duration' style='float: right'>" + $meta['numfiles'] + "</span></td> \
            <td class='action' " + toggleJs + "><button class='icon'> </button></td> \
        </tr>";
    return htmltext;
};

function showFile($meta) {
    // display basename without path and file extension
    var diff = dateDiff($meta['lastPlayed']);
    htmltext = "<tr> \
            <td class='type'> \
            <div class='preview'><a href='/alert/trigger.sh?preview=" + $meta['path'] + "' target='mediaPlay'>" + readableBytes($meta['size']) + "</a></span> \
        </td> \
        <td class='title'> \
            <span class='plays'>" + (($meta['plays'] > 0) ? $meta['plays'] : '') + "</span> \
            <a href='/alert/trigger.sh?preview=" + $meta['path'] + "' target='mediaPlay' class='trigger'><span class='file'>" + $meta['basename'] + "</span></a> \
            </td> \
            <td class='date " + ($meta['plays'] > 0 ? 'played' : 'created') + "'> \
               <span class='date' data-focus='" + (diff < 1800 ? "1" : "") + "' data-date='" + $meta['date'] + "'>" + convertTime(dateDiff($meta['date']), 'short') + "</span> \
            </td> \
        <td class='bitrate'><span class='bitrate'>" + (Math.floor($meta['bitrate'] / 1000)) + "</span></td> \
        <td class='duration'><span class='duration' style='float: right'>" + (Math.floor($meta['duration'] / 100) / 10) + "</span></td> \
            <td class='action'><a href='javascript:triggerSound(\"" + $meta['path'] + "\");' onclick='this.parentElement.classList.add(\"active\");'> \
            <span class='actionType'>" + $meta['type'] + "</span><span class='icon active'>&#xF16B;</span></a></span> \
            </tr>";
    return htmltext;

};
function updateList(fullUpdate) {
    if (fullUpdate) {
        showDirectoryListing(true);
    } else {
        now = Math.floor((new Date()) / 1000);
        var els = document.querySelectorAll('[data-focus="1"]');
        for (var i = 0; i < els.length; i++) {
            el = els[i];
            /* only refresh elements that have actually changed */
            if (now > el.getAttribute('data-refresh')) {
                diff = dateDiff(el.getAttribute('data-date'));
                el.textContent = convertTime(diff, 'short');
                el.setAttribute('data-refresh', now + (diff > 60 * 60 ? 60 * 60 - 1 : (diff > 60 ? 59 : 0) + 1));
                if (diff >= parseInt(getSetting('QUOTA_REPETITION'))) {
                    el.parentElement.parentElement.classList.remove('quota-violation-repetition');
                    el.setAttribute('data-focus', 0);
                }
            }
        }
    }
}
function frame() {
    var client = new XMLHttpRequest();
    client.open('GET', '/alert/last-event.sh');
    client.onload = function () {
        if (this.responseText !== Document.lastEvent) {
            Document.lastEvent = this.responseText;
            showDirectoryListing(true);
        } else {
            updateList();
        }
    };
    client.send();
}
function showDirectoryListing(update) {
    var client = new XMLHttpRequest();
    client.open('GET', '/alert/data.sh');
    client.onreadystatechange = function () {
        Document.data = client.responseText;
    };
    client.onload = function () {
        initList(update);
        if (!update) {
            window.setInterval(function () {
                updateList()
            }, 1000);
            window.setInterval(function () {
                showDirectoryListing(true)
            }, 120000);
        }
    };
    client.send();
}
function search(searchterm) {
    var els, i, results, dirs = 0, files = 0, files_search = 0, dirs_search = 0;
    if (!searchterm) {
        if (pwd) pwd.innerHTML = "sounds";
        els = document.querySelectorAll('tr.directory, tr.file');
        for (i = 0; i < els.length; i++) {
            el = els[i];
            if (el.classList.contains('directory')) {
                el.classList.remove('hide');
                el.classList.remove('no-matches');
                dirs++;
            } else {
                files++;
                el.classList.add('hide');
            }
            el.classList.remove('search-match');
            el.classList.remove('grayout');
        }
    } else {
        document.getElementById('searchterm').value = "";
        pwd = document.getElementById('pwd');
        els = document.querySelectorAll('tr.directory');
        for (i = 0; i < els.length; i++) {
            el = els[i];
            el.classList.add('no-matches');
        }
        els = document.querySelectorAll('tr.file');
        results = 0;
        for (i = 0; i < els.length; i++) {
            el = els[i];
            compare = el.id;
            if (compare.indexOf(searchterm) > -1) {
                files_search++;
                el.classList.add('search-match');
                el.classList.remove('grayout');
                parentDir = el.getAttribute('data-parent-dir');
                document.getElementById('path-' + parentDir).classList.remove('no-matches');
                results++;
            } else {
                el.classList.remove('search-match');
                el.classList.add('grayout', 'hide');
            }
        }
        // show meta and summary info
        if (pwd) pwd.innerHTML = '<span class="amount">' + results + '</span> Search Results for <span class="searchterm">"' + searchterm + '"</span>';
    }
    updateList(true);
}
function networkScan(rescan) {
    var client = new XMLHttpRequest();
    client.open('GET', '/alert/network.sh' + (rescan ? '?rescan=1' : ''));
    console.log('triggering network scan');
    client.onload = function () {
        data = this.responseText.split(/[\n\r]/).filter(function(x){
            return (x !== (undefined || null || ''));
        });;
        el = document.getElementById('networkChannel');
        while (el.options.length) el.options.remove(0);
        console.log('found ' + data.length + ' networks:');
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            var option = document.createElement("option");
            var txt = data[i].split(" ")[1] ? data[i].split(" ")[1] : data[i].split(" ")[0];
            if (txt) {
                option.text = txt;
                el.add(option);
            }
        }
    };
    client.send();
}
window.addEventListener('load', function () {
    var delay = (function () {
        timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    document.getElementById('searchterm').onkeyup = function () {
        document.getElementById('searchterm').autocomplete = "off";
        delay(function () {
            searchterm = document.getElementById('searchterm').value;
            if (searchterm) search(searchterm);
        }, 3000);
    };
    document.getElementById('searchterm').on = function () {
        clearTimeout(timer);
    };
    networkScan();
    // document.getElementById('searchterm').onchange = function(){delay(function() {search(this.value);},3000);};
});
showDirectoryListing();

var domIsReady = (function(domIsReady) {
    var isBrowserIeOrNot = function() {
        return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
    };

    domIsReady = function(callback) {
        if(callback && typeof callback === 'function'){
            if(isBrowserIeOrNot() !== 'ie') {
                document.addEventListener("DOMContentLoaded", function() {
                    return callback();
                });
            } else {
                document.attachEvent("onreadystatechange", function() {
                    if(document.readyState === "complete") {
                        return callback();
                    }
                });
            }
        } else {
            console.error('The callback is not a function!');
        }
    };
    return domIsReady;
})(domIsReady || {});

(function(document, window, domIsReady, undefined) {
    domIsReady(function() {
        document.getElementById("pitch").addEventListener("change", function () {
            document.getElementById("pitch-label").innerText = this.value.toString();
        });
    });
})(document, window, domIsReady);
