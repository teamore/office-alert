#!/bin/bash
#set -x

source ../../etc/config.sh								#include config.sh


START=$SECONDS

PATH_TMP_UPLOAD="$PATH_UPLOAD"/"upload.tmp-""$$"					#path to temporary upload fill - PID is used for unique name
echo "" > "$PATH_TMP_UPLOAD"

#----recieve html form upload--------------------------------------------------

(while read -r -d ''; do								#read until next NUL
        printf %s'\0' "${REPLY}";							#print NUL
done;
printf %s "${REPLY}"
) >> $PATH_TMP_UPLOAD

TIMESTAMP_A=$SECONDS

if [ $(wc -c < $PATH_TMP_UPLOAD ) -le 1 ]; then
	rm $PATH_TMP_UPLOAD
	exit 0
fi

#----upload done---------------------------------------------------------------

function get_line (){                                                                   #get_line writes the pointed line from file to $line
        line=$( sed -n "$line_number"p "$PATH_TMP_UPLOAD" )
}

function remove_line (){
	sed -i "$line_number"d "$PATH_TMP_UPLOAD"					#remove_line removes the pointed line from file
}

values=''										#values stores all header information and arguments
eof_flag="unset"
skip_input_flag="unset"									#used to skip the soundfile argument
line_number=1										#pointer at current line

remove_line
get_line
hash=${line##*-}									#get header hash

while [ $eof_flag = "unset" ]; do							#loop over all input elements

#----decode header-------------------------------------------------------------

        remove_line									#remove hash line
	get_line									#extract header line
        help=$( echo "${line#*; }" | sed 's/"//g' )					#
        values="$values%$help"								#
	remove_line									#remove header line
        line=$( sed -n "$line_number"p "$PATH_TMP_UPLOAD" | cut -c1-13 )		#check for file upload
        if [ $line ];then
                if [ $line = "Content-Type:" ]; then
                        skip_input_flag="set"						#if argument = "Content-Type:*" set flag to skip it
			remove_line
                fi
        fi

#----get argument--------------------------------------------------------------

        if [ $skip_input_flag = "unset" ]; then
                remove_line								#remove empty line
                get_line								#get arg
                values="$values?$line"							#
		remove_line 								#remove arg line
        fi

	if [ $skip_input_flag = "set" ]; then						#skip arg if it's the filecontent
		remove_line								#remove empty line
		skip_input_flag="unset"							#reset flag
	fi

#----skip to next hash---------------------------------------------------------

	get_line									#format line
	line=$( echo "$line" | sed 's/-//g' )						#

	i=1
	while [ ! "$line" = "$hash" ]; do
		line_number=$( grep -a -n -m $i ^- "$PATH_TMP_UPLOAD" | cut -d ":" -f1 | tail -n1 )
		get_line								#loop over all lines starting with "-"
	        line=$( echo "$line" | sed 's/-//g' )					#to find next hash
		i=$( expr $i + 1 )
	done

	get_line
	line=$( echo "$line" | sed 's/\r//g' )						#check if line = EOF
	line=$( echo "$line" | tail -c3 )						#
											#
	if [ "$line" = "--" ]; then							#
		eof_flag="set"								#set EOF flag
	fi										#
done

#----remove last header lines--------------------------------------------------

remove_line										#remove last header pieces
file_length=$( wc -c "$PATH_TMP_UPLOAD" )						#
file_length=${file_length%% *}								#
file_length=$( expr $file_length - 2 )							#
truncate -s $file_length "$PATH_TMP_UPLOAD"						#

values=$( echo "$values" | sed 's/; /\&/g' )						#format values
values=$( echo "$values" | sed 's/\r//g' )						#

TIMESTAMP_B=$SECONDS

#----end decoder---------------------------------------------------------------
#
#  structure of $values
#
#   	 %([header1_term1]?[arg1])%([header2_term1]?[arg2])%([header3_term1&header3_term2])
#
#  example for this specific structure
#
#	values='%name=filename?test.wav%name=path?Schlager%name=upload&filename=test.mp3'
#
#----get arguments from string-------------------------------------------------

values=$( echo "$values" | sed 's/%/ /g' )						#format values
values=$( echo "$values" | sed 's/\&/ /g' )						#

for i in $values; do									#extract all used arguments from values
											#
        case ${i%%\?*} in								#
											#
                name=path)								#
                FOLDER="${i##*\?}"							#
                ;;									#
                name=filename)								#
                NAME="${i##*\?}"							#
                ;;									#
               	filename=*)								#
             	DEFAULT_NAME="${i##*=}"							#
                ;;									#
		name=rsync)								#
		RSYNC_FLAG="${i##*\?}"							#
		;;									#
		*)									#
                ;;									#
        esac										#
done											#

#----save file ----------------------------------------------------------------

if [ -z $NAME ]; then									#use default name if there is no name argument
        NAME="$DEFAULT_NAME"
fi

FOLDER=$( echo "$FOLDER" | sed 's/\///g' )						#remove slashes from userinput
NAME=$( echo "$NAME" | sed 's/\///g' )							#

if [ $DEFAULT_NAME ]; then							#catches no upload

	if [[ $NAME = *.mp3 ]] || [[ $NAME = *.wav ]] || [[ $NAME = *.ogg ]]; then	#catches forbidden names

		if [ -d "$PATH_SOUNDS"/"$FOLDER" ]; then				#catches folder not existing

			if [ ! -f "$PATH_SOUNDS"/"$FOLDER"/"$NAME" ]; then		#catches file existing

				mv "$PATH_TMP_UPLOAD" "$PATH_SOUNDS"/"$FOLDER"/"$NAME"
				/cache-update.sh "$FOLDER"/"$NAME"
				line1="Upload successful"
				line2="Saved upload at "$PATH_SOUNDS"/"$FOLDER"/"$NAME""

			else
				rm "$PATH_TMP_UPLOAD"
				line1="Upload failed"
				line2="The file "$PATH_SOUNDS"/"$FOLDER"/"$NAME" already exists"
			fi
		else
			mkdir "$PATH_SOUNDS"/"$FOLDER"/
			mv "$PATH_TMP_UPLOAD" "$PATH_SOUNDS"/"$FOLDER"/"$NAME"
			/cache-update.sh "$FOLDER"/"$NAME"
			line1="Upload successful"
			line2="Created new directory "$PATH_SOUNDS"/"$FOLDER"/"
			line3="Saved upload at "$PATH_SOUNDS"/"$FOLDER"/"$NAME""
		fi
	else
		rm "$PATH_TMP_UPLOAD"
        	line1="Upload failed"
        	line2="The name should be *.(mp3|wav|ogg)"
	fi

else
	rm "$PATH_TMP_UPLOAD"
	line1="There was no file selected to upload"
fi

#----rsync---------------------------------------------------------------------

if [ $RSYNC_FLAG ]; then

	for i in "${!HOSTS[@]}"; do							#loop over HOSTS except own IP
		if [ ! $(ifconfig | grep -m 1 inet | cut -d: -f2 | cut -d' ' -f1) = "$i | cut -d, -f1" ]; then
			rsync --exclude=*.sh -aze 'ssh -i /opt/epubli/office-alert/etc/id_rsa' $PATH_SOUNDS root@$i:$PATH_UPLOAD/
		fi
	done
	line4="Synchronised all PI's sound library with "${USER_SETTINGS[HOST]}""
fi

#----output--------------------------------------------------------------------

TIME_UPLOAD=$( expr $TIMESTAMP_A - $START )                                             #calculate timer
TIME_DECODER=$( expr $TIMESTAMP_B - $TIMESTAMP_A )

echo "Content-type: text/html"
echo ""
echo "<html><head><title>Office-Alert Fileupload</title></head>"
echo "<body>"
echo "<p>"$line1"</p>"
echo ""
echo "<p>"$line2"</p>"
echo ""
echo "<p>"$line3"</p>"
echo ""
echo "<p>"$line4"</p>"
echo ""
echo "<p>""Upload time: "$TIME_UPLOAD" Decode time: "$TIME_DECODER"</p>"
echo "</body></html>"

exit 0
