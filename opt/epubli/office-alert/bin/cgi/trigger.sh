#!/bin/bash

cd "$(dirname "$0")"

source init.sh

ERROR_CODE=0

OIFS="${IFS}"
IFS="+, "

CAPTION=${settings['caption']}      # optional caption to be displayed on the stdout
CHANNEL=(${settings['channel']})    # optional parameter to specify additional targets
DUE=${settings['due']}              # specifies an optional timestamp to start playback at
DELAY=${settings['delay']}          # specifies an optional delay after which playback is started
DEVICE=${settings['device']}        # specifies which device is used for playback
DURATION=${settings['duration']:-"1"}    # defines how long (in seconds) the alert lamps are to be glowing
LANGUAGE=${settings['language']}    # text2speech language [default: de]
LIGHT=(${settings['light']})        # specifies the alert light(s) to be triggered
MESSAGE=${settings['message']}      # optional message to be displayed on the stdout
PITCH=${settings['pitch']}          # optional pitch value
PREVIEW=${settings['preview']}
REPEAT=${settings['repeat']:-"1"}   # specifies an optional repetition amount (default: 1)
SOUND=${settings['sound']}          # specifies the sound file name to be played back
SPEECH=${settings['speech']}        # text2speech
STOP=(${settings['stop']})          # stops playback
SWITCH=${settings['switch']:-"1"}   # sends a switch signal to the remote
SYSTEMCODE=${settings['code']:-"$SYSTEMCODE"}
VERBOSE=${settings['verbose']}      # verbosity
VOLUME=${settings['volume']}        # optional volume
APPEND=${settings['append']}        # additional parameters (will be appended) 

NOW=$((10#`date +%s`))
DURATION_VALID=`echo "$DURATION 0" | awk '{print ($1 >= $2)}'`

IFS=$OIFS

HEADER_STATUS_CODE="200"
HEADER_CONTENT_TYPE="text/plain"
OUTPUT=""
ERROR_CODE=0

# set mixer preferences
execute sudo $PATH_AMIXER cset numid=$DEVICE $VOLUME

if [[ $PREVIEW ]]; then
    bash download.sh "$PREVIEW"
    exit 0
fi

if ( [ "$CHANNEL" ] ); then
    if ! ( [ "$DUE" ] ); then
        DUE=$(echo `date +%s`+$SYNC_BUFFER | bc -l)
    fi

    params=""

    # urlencode all parameter values
    settings["due"]="$DUE"
    for i2 in ${!settings[@]}
    do
        settings["$i2"]=${settings["$i2"]//[ ]/+}
        if ! [[ "$i2" == "channel" ]]; then
            params=$params" --data-urlencode $i2=${settings[$i2]}"
        fi
    done

    echo "Status: 202"
    echo "Content-type: ${HEADER_CONTENT_TYPE}"
    echo ""

    for chan in "${CHANNEL[@]}"
    do
        URL=$chan$URI_CGI'trigger.sh'
        # trigger actual http request
        echo "curl -G $URL $params &"
        curl -G $URL $params &
    done
    exit 0
fi


if ( [ -n "$STOP" ] || ( [ -z "$STOP" ] && [ "${STOP+xxx}" == "xxx" ] ) ) ; then
    source stop.sh
fi

# play back sounds
if [[ "$SOUND" && -f "$PATH_SOUNDS/$SOUND" ]];
then
    if [[ -f "$FILE_DATA" ]]; then
            DB_ENTRY=`cat "$FILE_DATA" | grep --text "^$SOUND:.*$" | grep -o ':.*' | cut -c 2-`
            IFS=, read plays lastPlay <<< $DB_ENTRY
    fi

    type=`mediainfo --inform="Audio;%Format%" "$PATH_SOUNDS/$SOUND"`

    # ignore all quotas (e.g. for system notification sounds) residing underneath the designated folder
    if ! [[ "$SOUND" == "$QUOTA_IGNORE_PATH"* ]] ; then

        if [[ -f "$FILE_QUOTA" ]]; then
            QUOTA=`cat $FILE_QUOTA`
            if [[ ${NOW} -lt ${QUOTA} ]]; then
                OUTPUT="${OUTPUT}\nERROR: Trigger Quota Limit [$QUOTA_TRIGGER_LIMIT/$QUOTA_TRIGGER_PERIOD] exceeded. Please wait ["$(($QUOTA-$NOW))"] second(s)"
                ERROR_CODE=8
                HEADER_STATUS_CODE="429"
                SOUND=""
            fi
        fi
        # check if repetition quota applies
        if [[ $QUOTA_REPETITION -gt 0 && $lastPlay ]]; then
            quotaTimeout=$((10#$lastPlay+$QUOTA_REPETITION))
            if [[ ${NOW} -lt ${quotaTimeout} ]]; then
                OUTPUT="${OUTPUT}\nERROR: Repetition Quota \[$QUOTA_REPETITION sec / sound] exceeded. Please wait \["$(($quotaTimeout-$NOW))"] second(s)"
                ERROR_CODE=9
                HEADER_STATUS_CODE="429"
                SOUND=""
            fi
        fi
    fi
elif [[ "$SOUND" ]]; then
    OUTPUT="${OUTPUT}\nERROR: File ['$PATH_SOUNDS/$SOUND'] does not exist or access is prohibited (check permissions)."
    SOUND=""
    HEADER_STATUS_CODE="404"
    ERROR_CODE=1
fi

if [ "$DUE" ]; then
    diff=$(echo $DUE-`date +%s.%N` | bc -l)
    RESULT=$RESULT"\n[$(hostname)] ready for scheduled simultaneous playback in ["$diff" seconds]. standing by.."
    sleep $diff
fi

if ( [[ "$SOUND" ]] );
then
    if ( [[ "$PATH_MPLAYER" ]] ); then
        params=""
        af=""
        if ( [[ "$VOLUME" ]] ); then
            af="$af,volume=$VOLUME"
        fi
        if ( [[ "$PITCH" ]] ); then
            af="$af,scaletempo=scale=$PITCH:speed=pitch"
            params="-speed $PITCH $params"
        fi
        if ( [[ "$af" ]] ); then
            af="-af ${af:1:${#af}-1} "
        fi
        for (( c=1; c<=$REPEAT; c++ ))
        do
            if ( [[ "$DELAY" ]] ); then
                execute sleep "$DELAY"
            fi
            execute sudo $PATH_MPLAYER "$PATH_SOUNDS/$SOUND" $af$params $APPEND
        done
    else
        for (( c=1; c<=$REPEAT; c++ ))
        do
            if ( [[ "$DELAY" ]] ); then
                sleep "$DELAY"
            fi
            # use correct binary (wav,mpeg,ogg etc) for playback
            case "$type" in
                "PCM")
                    execute sudo $PATH_PLAYWAV $APPEND $PATH_SOUNDS/$SOUND
                ;;
                "Vorbis")
                    execute sudo $PATH_PLAYOGG $PITCH $APPEND $PATH_SOUNDS/$SOUND
                ;;
                "MPEG Audio")
                    execute sudo $PATH_PLAYMP3 $PITCH $APPEND $PATH_SOUNDS/$SOUND
                ;;
                *)
                    OUTPUT="{$OUTPUT}\nERROR: File ['$PATH_SOUNDS/$SOUND'] has an unknown media type and cannot be played back."
                    HEADER_STATUS_CODE="403"
                    SOUND=""
                    ERROR_CODE=3
                    exit $ERROR_CODE
                ;;
            esac
        done
    fi
    if [[ ${ERROR_CODE} -eq 0 ]]; then
        RESULT=$RESULT'\n['$(hostname)'] triggered sound file ['$SOUND'] at '`date +%H:%M:%S.%N`

        # delete data entry for the respective sound file
        ex +g@$SOUND@d -cwq $FILE_DATA

        # increase '$plays' counter
        plays=${plays:-0}
        let plays+=1
        # generate current timestamp
        # persist total $plays and $lastPlayed timestamp
        printf "$SOUND:$plays,$NOW\n" >> $FILE_DATA

        soundsPlayedInQuotaPeriod=$((10#0))
        # check if global quota limit is exceeded
        IFS=$'\t'
        dates=( $(tail -n$QUOTA_TRIGGER_LIMIT /opt/epubli/office-alert/var/data/sounds.meta.data | grep -o ',.*$' | cut -c2-) )
        earliest=''
    fi

    while read -r date; do
        if [ -z "$earliest" ]; then
            earliest="$date"
        fi
	    timeout=`echo "$date+$QUOTA_TRIGGER_PERIOD" | bc -l`
        if [[ "$timeout" -lt "$NOW" ]]; then
            break
        fi
        let soundsPlayedInQuotaPeriod+=1
    done <<< "$dates"

    if ! [[ $soundsPlayedInQuotaPeriod -lt $QUOTA_TRIGGER_LIMIT ]]; then
        earliest=$(echo "$earliest+$QUOTA_TRIGGER_PERIOD" | bc -l)
        HEADER_STATUS_CODE="429"
        OUTPUT="${OUTPUT}\nNOTICE: Quota Limit reached! Playback locked for "`echo "$earliest-$NOW" | bc -l` seconds
	    OUTPUT="${OUTPUT}\n$earliest > \"$FILE_QUOTA\""
    fi
fi

if [[ "$SPEECH" ]];
then
    RESULT=$RESULT"\n"`bash speech.sh "$SPEECH" > /dev/null 2>&1 &`
fi

if ( [ "$DURATION_VALID" == "1" ] || [ "$DURATION" == "" ] );
then
    # trigger alert lamps
    for i in "${LIGHT[@]}"
    do
        RESULT=$RESULT"\n"`sudo $PATH_SENDSCRIPT -p 8 $SYSTEMCODE $i $SWITCH > /dev/null 2>&1`
    done
fi

if (( $(echo "$DURATION 0" | awk '{print ($1 > $2)}') == 1 )); then
    # pause duration
    sleep $DURATION

    # turn off alert lamps
    for i in "${LIGHT[@]}"
    do
        RESULT=$RESULT"\n"`sudo $PATH_SENDSCRIPT -p 8 $SYSTEMCODE $i 0 > /dev/null 2>&1`
    done
fi

IFS=$OIFS
END=$(date +%s.%N)
DIFF=`echo "$END-$START" | bc -l`
#DIFF=`printf %0.4f $DIFF`
OUTPUT=`echo -e "${OUTPUT}\n${RESULT}"`

echo "Status: ${HEADER_STATUS_CODE}"
echo "Content-type: ${HEADER_CONTENT_TYPE}"
echo "X-Execution-Time: ${DIFF}"
echo "X-ExitCode: ${ERROR_CODE}"
echo ""
echo ${OUTPUT}

if [[ "$VERBOSE" ]];
then
    echo "Settings:"
    echo ${settings[@]}
    echo "Log:"
    echo -e ${LOG}
fi

exit $ERROR_CODE
