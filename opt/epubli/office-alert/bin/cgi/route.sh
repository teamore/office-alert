#!/bin/bash

source init.sh

# echo http content header
echo "Content-type: text/html"
echo ""

declare -A hosts

# iterate through all available parameters and populate the host array accordingly
while IFS== read key value
do
    # we store all host-settings in the array $hosts
    if [[ "$key" =~ master|slave ]]; then
        hosts["$key"]="$value"
    fi
done < <(echo "$q0" | sed 's/&/\n/g' )

IFS="$OIFS"

# initialize array $current_host
declare -A current_host
defaultTrigger=$URI_CGI'trigger.sh'

if [[ "${settings[due]}" ]]; then
    DUE="${settings[due]}"
else
    # 2 seconds sync buffer
    DUE=$(echo `date +%s`+$SYNC_BUFFER | bc -l)
fi

# iterate through all available hosts and trigger http requests to all available alert devices
for i in ${!hosts[@]}
do
    # split parameter values by delimiter ","
    data=${hosts[$i]}
    buf=(${data//,/ })
    # reset $current_host
    current_host=()
    for i2 in ${!buf[@]}
    do
        # split keys from values by delimiter "="
        data=${buf[$i2]}
        buf2=(${data//=/ })
        # store key-value-pairs in $current_host
        current_host[${buf2[0]}]=${buf2[1]}
    done
    # assemble http request url
    url="${current_host["host"]}/${current_host["trigger"]:-$defaultTrigger}"
    # drop all sound-related parameters if "silent=1"-parameter is present
    if [[ "${current_host["silent"]}" == 1 ]]; then
        if [[ ${settings['light']} ]]; then
            params="light=${settings['light']}&duration=${settings['duration']}"
            echo "curl $url?$params"
            # trigger actual http request
            curl $url?$params &
            # trigger sound + lights otherwise
        fi
    else
        params=""
        IFS=$'\n'
        settings["due"]=$DUE
        # drop the "channel"-parameter as this may cause adverse side-effects w$
        unset "settings['channel']"
        # urlencode all parameter values
        for i2 in ${!settings[@]}
        do
            data=${settings[$i2]}
            data="${data// /+}"
            params=$params" --data-urlencode $i2=$data"
        done
        echo "curl -G $url $params &"
        # trigger actual http request
        IFS="$OIFS"
        curl -G $url $params &
    fi
done
