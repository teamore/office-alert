#!/usr/bin/env bash

declare -A CACHE
declare -A DATA

OIFS=$IFS
IFS=$'\n'
# read cache data
readarray CACHE_DATA < $FILE_CACHE
for buf in ${CACHE_DATA[@]}
do
    # iterate through all available parameters and populate the data arrays accordingly
    IFS=: read key value <<< $buf
    CACHE[$key]=$value
done

# read meta data
readarray DATA_DATA < $FILE_DATA

for buf in ${DATA_DATA[@]}
do
    # iterate through all available parameters and populate the data arrays accordingly
    IFS=: read key value <<< $buf
    DATA[$key]=$value
done
IFS=$OIFS
