#!/usr/bin/env bash

source init.sh

echo "Content-Type: text/plain; charset=utf-8"
echo ""

if ([[ -f "$FILE_NETWORK" ]] && ! [[ "${settings['rescan']}" ]] ); then
    cat "$FILE_NETWORK"
    exit 0
fi

# GET CURRENT NETWORK IP ADDRESSES
IPs=`sudo "$PATH_BASH/network-info.sh"`

cIP=($IPs)
environment=`echo ${cIP[0]} | cut -d. -f1,2,3`

IFS=$'\n'

nmap=`nmap $environment.* -p80 -oG - | awk '/Status/{print $2,substr($3,2,length($3)-2),$5}'`
readarray -t hosts <<<"$nmap"

data=""

for index in "${!hosts[@]}"
do
    IFS=" " read -r -a seg <<< ${hosts[index]}
    testURL=${seg[0]:-$seg[1]}"/ping.html"
    result=`curl -s -D - $testURL`
    readarray -t row <<<"$result"
    check=`echo ${row[0]} | grep "200"` && `echo $result | grep "^bazinga$"`
    if [[ "$check" ]]; then
        data=$data`echo -n ${hosts[index]}`
        data=$data" Ok"
        data=$data$'\n\r'
#    else
#        data=$data`echo -n ${hosts[index]}`
#        data=$data" -"
#        data=$data$'\n\r'
    fi
done
echo $data > $FILE_NETWORK
echo $data