#!/usr/bin/env bash

source init.sh
source meta.sh
source $PATH_BASH/date-diff.sh ""
source $PATH_BASH/convert-time.sh ""

# echo http content header
echo "Content-type: text/html"
echo ""
echo '<link rel="stylesheet" href="/css/style.css">'

echo "<table>"

echo "<tr>"
echo "<th>Type</th>"
echo "<th>Name</th>"
echo "<th>Last Access</th>"
echo "<th>Bitrate</th>"
echo "<th style='text-align: right;'>Duration</th>"
echo "<th>Play</th>"
echo "</tr>"

# create array clone
declare -A sorted
for URL in "${!DATA[@]}"
do
    IFS=, read plays lastPlay <<< ${DATA[$URL]}
    sorted["$URL"]="$lastPlay,$URL,$plays"
done
# sort the array clone by first column (lastPlay)
IFS=$'\n'
sorted_str=`sort -r <<<"${sorted[*]}"`
IFS=$'\n'
# reassign
array=(`echo "$sorted_str"`)

for key in "${!array[@]}"
do
    IFS=, read lastPlay URL plays <<< ${array[$key]}
	case $URL in
        *.*)
            FULL_URL="$URL"

            FILE="$PATH_SOUNDS/$FULL_URL"
            css_class=""
            # is cached media info available?
            if [ ${CACHE[$FULL_URL]+abc} ]; then
                IFS=, read type bitrate duration created lastAccess size <<< ${CACHE[$FULL_URL]}
                css_class="cache"
            else
                MINF=`mediainfo --inform="Audio;%Format%,%BitRate%,%Duration%" "$FILE"`
                IFS=, read type bitrate duration <<< $MINF
                MINF=`stat "$FILE" -c%y,%x,%s`
                IFS=, read created lastAccess size <<< $MINF

                printf "$FULL_URL:$type,$bitrate,$duration,$created,$lastAccess,$size\n" >> $FILE_CACHE
                css_class="no-cache"
            fi
            bitrate=$((bitrate/1000))

            dateContext='created'
            quota=''
            if [[ $lastPlay ]]; then
                dateContext='played'
                lastPlayDiff=`date_diff "$lastPlay"`
                secondsAgo=`date_diff "$lastPlay" "" "num"`
                if [[ $secondsAgo -lt $QUOTA_REPETITION ]]; then
                    quota="quota-violation repetition"
                fi
            else
                lastPlayDiff=`date_diff "$created"`
            fi
            case $type in
                "PCM")
                    css_class=$css_class" wav"
                    type="wav"
                ;;
                "MPEG Audio")
                    css_class=$css_class" mp3"
                    type="mp3"
                ;;
                "Vorbis")
                    css_class=$css_class" ogg"
                    type="ogg"
                ;;
                *)
                    type=`echo "$type" | tr '[:upper:]' '[:lower:]'`
                    css_class=$css_class" $type"
                    type="$type"
                ;;
            esac

            echo "<tr class='$css_class $quota'>"
            echo "<td class='type'>"
            echo "<div class='preview'><a href='trigger.sh?preview=$FULL_URL'>$type</a></span>"
            echo "</td>"
            echo "<td class='title'>"
            echo "<span class='plays'>$plays</span>"
            echo "<a href='trigger.sh?preview=$FULL_URL' class='trigger'><span class='file'>$URL</span></a>"
            echo "</td>"
            echo "<td class='date $dateContext'><span class='date'>$lastPlayDiff</span></td>"
            echo "<td class='bitrate'><span class='bitrate'>$bitrate kbps</span></td>"
            echo "<td class='duration'><span class='duration' style='float: right'>`convert_time $duration`</span></td>"
            echo "<td class='action'><a href='trigger.sh?sound=$FULL_URL'><span>play</span></a></span>"
            echo "</tr>"
        ;;
    esac
done
echo "<tr class='summary'><td>"
echo "<span class='amount'>"${#array[@]}"</span>"
echo "</td>"
echo "<td colspan='5'>"
echo "<span>Total Results</span>"
echo "</td>"
echo "</tr>"
echo "</table>"

source footer.sh
exit 0