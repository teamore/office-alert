#!/usr/bin/env bash

source init.sh
GUESS=$((1 + RANDOM % 10))
if [ $GUESS == ${settings['guess']} ]; then
	sudo /usr/bin/killall mpg123
	sudo /usr/bin/killall ogg123
	sudo /usr/bin/killall aplay
	sudo /usr/bin/killall mplayer
fi
echo "Status: 200"
echo "Content-type: text/plain"
echo "X-ExitCode: 0"
echo ""
if [ $GUESS == ${settings['guess']} ]; then
	echo "Your guess $GUESS was right!"
	echo "[$(hostname)] stopped all sounds currently playing back."
else
	echo "Sorry ${settings['guess']} is wrong. It was $GUESS."
	echo "Sounds will not be stopped! Please try again guessing a number between 1 and 10."
	echo "Add ?guess=1 to this URL to guess 1."
fi
exit 0