#!/usr/bin/env bash

soundsPlayed=0
lastPlayed=-1
css_class=''
# check if global quota limit is exceeded
for URL in "${!DATA[@]}"
do
    read plays lastPlay <<< ${DATA[$URL]}
    secondsAgo=`date_diff "$lastPlay" "" "numeric"`
    if [[ $secondsAgo -lt $lastPlayed || $lastPlayed == -1 ]]; then
        lastPlayed=$secondsAgo
    fi
    if [[ $secondsAgo -lt $QUOTA_TRIGGER_PERIOD ]]; then
        let soundsPlayed+=1
        if [[ $soundsPlayed -gt $QUOTA_TRIGGER_LIMIT ]]; then
            css_class='quota hit'
        fi
    fi
done
echo "<span class='$css_class'>Repetition quota: <strong>$QUOTA_REPETITION</strong> seconds | Trigger limit: <strong>$soundsPlayed</strong> / <strong>$QUOTA_TRIGGER_LIMIT</strong> ($QUOTA_TRIGGER_PERIOD seconds)</span>"
exit 0
