#!/usr/bin/env bash

source init.sh

# echo http content header
echo "Content-type: text/plain-text"
echo "Content-Disposition: inline"
echo ""

PATH_CURRENT=${settings["path"]:-""}
length=${#PATH_CURRENT}
last_char=${PATH_CURRENT:length-1:1}

[[ $last_char != "/" ]] && PATH_CURRENT="$PATH_CURRENT/"; :

PATH_REAL="`realpath $PATH_SOUNDS/$PATH_CURRENT`"

if ([[ $PATH_REAL != *"$PATH_SOUNDS"* ]] || ! [ -e "$PATH_REAL" ]); then
   PATH_CURRENT="./"
   PATH_REAL="`realpath $PATH_SOUNDS/$PATH_CURRENT`"
fi

opwd=`pwd`
cd $PATH_SOUNDS/$PATH_CURRENT
find * -maxdepth 1;
echo -e '\n'

# GET CURRENT NETWORK IP ADDRESSES
IPS=`sudo "$PATH_BASH/network-info.sh"`

# APPLICATION SETTINGS
PATH_CURRENT=${settings["path"]:-""}
echo -e "HOSTNAME\t$(hostname)"
echo -e "PATH_CURRENT\t$PATH_CURRENT"
echo -e "HOST_IP\t$IPS"
echo -e "QUOTA_REPETITION\t$QUOTA_REPETITION"
echo -e "QUOTA_TRIGGER_LIMIT\t$QUOTA_TRIGGER_LIMIT"
echo -e "QUOTA_TRIGGER_PERIOD\t$QUOTA_TRIGGER_PERIOD"

for i in "${!HOSTS[@]}"
do
    echo -e "HOSTS[$i]\t"${HOSTS[$i]}
done

echo -e "\n"

declare -A FILE_SIZES
OIFS=$IFS
IFS=$'\n'
for row in $(du -sh ./*/); do
    path=`echo "$row" | cut -f2 | cut -c3- | sed 's/\/$//g' | head -c-1`
    echo -n $path" "
    echo -n "$row" | cut -f1 | tr '\n' ' '
    ls "$PATH_REAL/$path" | wc -l
done

echo -e '\n'
cat $FILE_CACHE
echo -e '\n'

cat $FILE_DATA
echo -e '\n'
IFS=$OIFS

exit 0
