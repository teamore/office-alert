#!/usr/bin/env bash

source ../../etc/config.sh

PARENT_COMMAND="$(ps -o comm= $PPID)"

if [[ $PARENT_COMMAND ]]; then
    echo "Content-type: text/html"
    echo ""
fi

# enforce update of given cache file entities
opwd=`pwd`
cd $PATH_SOUNDS
for FILE in $*; do
    FULL_PATH="$PATH_SOUNDS/$FILE"

    # delete data entry for the respective sound file
    ex +g@$FILE@d -cwq $FILE_CACHE
    if [ -r "$FULL_PATH" ]; then
        MINF=`mediainfo --inform="Audio;%Format%,%BitRate%,%Duration%" "$FULL_PATH"`
        IFS=, read type bitrate duration <<< $MINF
        MINF=`stat "$FULL_PATH" -c%y,%x,%s`
        IFS=, read created lastAccess size <<< $MINF
        printf "$FILE:$type,$bitrate,$duration,$created,$lastAccess,$size\n"
        printf "$FILE:$type,$bitrate,$duration,$created,$lastAccess,$size\n" >> $FILE_CACHE
    fi
done
cd $opwd
exit 0