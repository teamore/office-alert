#!/usr/bin/env bash

source init.sh

echo "Content-type: text/html"
echo ""

# get timestamp of last activity
tail -n1 $FILE_DATA | cut -d, -f2