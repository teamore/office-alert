#!/usr/bin/env bash

source init.sh
source meta.sh
source $PATH_BASH/convert-time.sh ""
source $PATH_BASH/date-diff.sh ""

bytesToHuman() {
    b=${1:-0}; d=''; s=0; S=(Bytes {K,M,G,T,E,P,Y,Z})
    while ((b > 1024)); do
        d="$(printf ".%01d" $((b % 1024 * 100 / 1024)))"
        b=$((b / 1024))
        let s++
    done
    #echo "$b$d ${S[$s]}"
    echo "$b${S[$s]}"
    exit 0
}

# echo http content header
echo "Content-type: text/html"
echo ""
echo '<link rel="stylesheet" href="/css/style.css">'

PATH_CURRENT=${settings["path"]:-""}
length=${#PATH_CURRENT}
last_char=${PATH_CURRENT:length-1:1}

[[ $last_char != "/" ]] && PATH_CURRENT="$PATH_CURRENT/"; :

PATH_REAL="`realpath $PATH_SOUNDS/$PATH_CURRENT`"

if ([[ $PATH_REAL != *"$PATH_SOUNDS"* ]] || ! [ -e "$PATH_REAL" ]); then
   PATH_CURRENT="./"
   PATH_REAL="`realpath $PATH_SOUNDS/$PATH_CURRENT`"
fi

declare -A FILES DIRS
filesizeParams='h'

if [ ${settings['all']} ]; then
    filesizeParams='h'
    opwd=`pwd`
    cd $PATH_SOUNDS/$PATH_CURRENT
    LIST="`find . | sort | cut -c3-`"
    cd $opwd
    FILES=$(echo $LIST | tr ',' "\n")
else
    LIST="`ls -p $PATH_SOUNDS/$PATH_CURRENT | grep '[/]'`"" "
    LIST+="`ls -p $PATH_SOUNDS/$PATH_CURRENT | grep -v /`"
    FILES=$(echo $LIST | tr ',' "\n")

fi

NUM=($FILES)

opwd=`pwd`
cd $PATH_REAL

declare -A FILE_SIZES
OIFS=$IFS
IFS=$'\n'
for i in `du -$filesizeParams *`; do
        IFS=$'\t' read size url <<< $i
        IFS=$'\n'
        FILE_SIZES["$url"]=$size
done;
cd $opwd
IFS=$OIFS

echo "<table>"
if [[ "$PATH_CURRENT" != "/" ]]; then
    echo "<tr class='directory root'>"
    echo "<th>"
    echo "<span class='preview'><a href='list.sh'>root</a></span>"
    echo "</th><th colspan=4>"
    echo "<a href='list.sh' class='trigger'>Back to Root Directory</a>"
    echo "</th></tr>"
fi


BASEDIR=$(basename "$PATH_REAL")

echo "<tr class='pwd'>"
echo "<th>.</th><th colspan=3>$BASEDIR</th>"
echo "<th class='upload'><a href='upload.sh?path=$PATH_CURRENT'><span class='upload'>upload</span></a></th>"
echo "</tr>"

echo "<tr>"
echo "<th>Size</th>"
echo "<th>Name</th>"
echo "<th>Last Access</th>"
echo "<th>Size</th>"
echo "<th style='text-align: right;'>Files</th>"
echo "<th>Action</th>"
echo "</tr>"
first=1
for URL in $FILES
do

    needle="/"
    depth=$(grep -o "$needle" <<< "$URL" | wc -l)

	case $URL in
        *.*)
            if [[ $first == 1 ]]; then
                first=0
                echo "<tr>"
                echo "<th>Size</th>"
                echo "<th>Name</th>"
                echo "<th>Last Access</th>"
                echo "<th>Bitrate</th>"
                echo "<th style='text-align: right;'>Duration</th>"
                echo "<th>Play</th>"
                echo "</tr>"
            fi
            FULL_URL="$PATH_CURRENT$URL"
            first_char=${PATH_CURRENT:0:1}

            [[ $first_char == "/" ]] && PATH_CURRENT=${PATH_CURRENT:1:}; :

            FILE="$PATH_SOUNDS/$FULL_URL"
            css_class=""
            # is cached media info available?
            if [ "${CACHE[$FULL_URL]+abc}" ]; then
                IFS=, read type bitrate duration created lastAccess size <<< ${CACHE[$FULL_URL]}
                css_class="cache"
            else
                bash cache-update.sh "$FULL_URL"
                css_class="no-cache"
                MINF=`mediainfo --inform="Audio;%Format%,%BitRate%,%Duration%" "$FILE"`
                IFS=, read type bitrate duration <<< $MINF
                MINF=`stat "$FILE" -c%y,%x,%s`
                IFS=, read created lastAccess size <<< $MINF
            fi
            bitrate=$((bitrate/1000))
            size=`bytesToHuman "$size"`

            IFS=, read plays lastPlay <<< ${DATA[$FULL_URL]}

            dateContext='created'
            quota=''
            if [[ $lastPlay ]]; then
                dateContext='played'
                lastPlayDiff=`date_diff "$lastPlay"`
                secondsAgo=`date_diff "$lastPlay" "" "num"`
                if [[ $secondsAgo -lt $QUOTA_REPETITION ]]; then
                    quota="quota-violation repetition"
                fi
            else
                lastPlayDiff=`date_diff "$created"`
            fi
            case $type in
                "PCM")
                    css_class=$css_class" wav"
                    type="wav"
                ;;
                "MPEG Audio")
                    css_class=$css_class" mp3"
                    type="mp3"
                ;;
                "Vorbis")
                    css_class=$css_class" ogg"
                    type="ogg"
                ;;
                *)
                    type=`echo "$type" | tr '[:upper:]' '[:lower:]'`
                    css_class=$css_class" $type"
                    type="$type"
                ;;
            esac

            echo "<tr class='$css_class depth$depth $quota'>"
            echo "<td class='type'>"
            echo "<div class='preview'><a href='trigger.sh?preview=$FULL_URL'>"${FILE_SIZES["$URL"]:-$size}"</a></span>"
            echo "</td>"
            echo "<td class='title'>"
            echo "<span class='plays'>$plays</span>"
            echo "<a href='trigger.sh?preview=$FULL_URL' class='trigger'><span class='file'>$URL</span></a>"
            echo "</td>"
            echo "<td class='date $dateContext'><span class='date'>$lastPlayDiff</span></td>"
            echo "<td class='bitrate'><span class='bitrate'>$bitrate kbps</span></td>"
            echo "<td class='duration'><span class='duration' style='float: right'>"`convert_time $duration`"</span></td>"
            echo "<td class='action'><a href='trigger.sh?sound=$FULL_URL'><span>$type</span></a></span>"
            echo "</tr>"
        ;;
        *)
            NFILES="`ls -1q "$PATH_REAL/$URL" | wc -l`"
            rsize=${FILE_SIZES["${URL::-1}"]:-$FILE_SIZES["$URL"]}
            rsize=${rsize:-"$size"}

            echo "<tr class='directory depth$depth'>"
            echo "<td class='type'>"
            echo "<div class='preview'><a href='list.sh?path=$URL'>$rsize</a></div>"
            echo "</td>"
            echo "<td class='title'>"
            echo "<a href='list.sh?path=$URL' class='trigger'>$URL</a>"
            echo "</td>"
            echo "<td class='date $dateContext' colspan=2><span class='date'>$lastPlayDiff</span></td>"
            echo "<td class='duration'><span class='duration' style='float: right'>$NFILES</span></td>"
            echo "<td class='action'><span class='cd'>open</span></td>"
            echo "</tr>"
        ;;
    esac
done
echo "<tr class='summary'><td>"
echo "<span class='amount'>"${#NUM[@]}"</span>"
echo "</td>"
echo "<td colspan='5'><span>Total Results</span></td>"
echo "</tr>"
echo "</table>"

source footer.sh
exit 0
