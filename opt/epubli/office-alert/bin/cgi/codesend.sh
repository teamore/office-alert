#!/usr/bin/env bash

source ../../etc/config.sh

PARENT_COMMAND="$(ps -o comm= $PPID)"

if [[ $PARENT_COMMAND ]]; then
    echo "Content-type: text/html"
    echo ""
fi

# enforce update of given cache file entities
opwd=`pwd`
cd $PATH_SOUNDS

for CODE in $*; do
    echo `sudo $PATH_CODESEND $CODE -l 198 -p 0 > /dev/null 2>&1`
    echo "Code [$CODE] has been sent"
done
cd $opwd
exit 0