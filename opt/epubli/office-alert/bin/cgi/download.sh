
#!/usr/bin/env bash

source init.sh
SOUND=${settings['preview']:-$1}

type=`mediainfo --inform="Audio;%Format%" "$PATH_SOUNDS/$SOUND"`
declare -A aliases
aliases["PCM"]="audio/wav"
aliases["MPEG Audio"]="audio/mpeg"
aliases["Vorbis"]="audio/ogg"
echo "Content-type: "${aliases["$type"]:-"audio"}
echo ""
cat $PATH_SOUNDS/$SOUND
exit 0
