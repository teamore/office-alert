#!/usr/bin/env bash

source ../../etc/config.sh

echo "Content-type: text/html"
echo ""

say()
{
        local IFS=+;
        sudo $PATH_MPLAYER -ao alsa -really-quiet -noconsolecontrols "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=$*&tl=de" > /dev/null 2>&1
}
say $*

echo "textToSpeech $*"
exit 0
