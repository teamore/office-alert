#!/usr/bin/env bash
START=$(date +%s.%N)
source ../../etc/config.sh

# initialization
OIFS="$IFS"
IFS="${IFS}&"

LOG=""

function execute() {
    $* > /dev/null 2>&1 &
    LOG="${LOG}\n$*"
}

# define data arrays# define data arrays
declare -A settings

if [[ $QUERY_STRING ]]; then
    # we are dealing with a query string (most likely triggered by cgi/web)
    set $QUERY_STRING
    ARGS="$*"
    IFS="$OIFS"

    # decode url parameters
    urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

    q0=$(urldecode $QUERY_STRING)

    # iterate through all available parameters and populate the data arrays accordingly
    while IFS== read key value
    do
        if ! [ -z "$key" ]; then
            settings["$key"]=$(urldecode "$value")
        fi
    done < <(echo "$q0" | sed 's/&/\n/g' )
else
    # we are probably dealing with a command line (cli)
    while [ "$1" != "" ]; do
        while IFS== read key value
        do
            if ! [ -z "$key" ]; then
                settings["$key"]="$value"
            fi
        done < <(echo "$1" | sed 's/&/\n/g' )
        shift
    done
fi
