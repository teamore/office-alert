#!/usr/bin/env bash
# must be run with root privileges
# iterate through all acccess logs in consecutive order
i=1
OUTPUT=''
while [ $i -gt -1 ]
    do
    if [[ -f "/var/log/apache2/access.log.$i" ]];
    then
        BUF=$(cat /var/log/apache2/access.log.$i | grep -E 'trigger.sh|route.sh' | grep 'sound' | cut -d' ' -f1,4,5,7 | sed 's/\[\(.*\?\)\]/\1/')

    elif [[ -f "/var/log/apache2/access.log.$i.gz" ]];
    then
        BUF=$(zcat /var/log/apache2/access.log.$i.gz | grep -E 'trigger.sh|route.sh' | grep 'sound' | cut -d' ' -f1,4,5,7 | sed 's/\[\(.*\?\)\]/\1/')
    else
        echo "$OUTPUT"
        cat /var/log/apache2/access.log | grep -E 'trigger.sh|route.sh' | grep 'sound' | cut -d' ' -f1,4,5,7 | sed 's/\[\(.*\?\)\]/\1/'
        exit 0
    fi
    OUTPUT=$BUF$OUTPUT
    i=$[$i+1]
done
