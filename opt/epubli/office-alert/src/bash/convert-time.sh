# human readable time
convert_time()
{
    drex='[0-9]*'

    if [[ $1 =~ $drex ]]; then
        t=$1
        local t=$((t/1000))

        local d=$((t/60/60/24))
        local h=$((t/60/60%24))
        local m=$((t/60%60))
        local s=$((t%60))
        t=$1
        local ms=$((t/100%10))

        if [[ $d > 0 ]]; then
                echo -n "$d:"
        fi
        if [[ $h > 0 ]]; then
                echo -n "$h:"
        fi
        if [[ $m > 0 ]]; then
                echo -n "$m:"
        fi
        if [[ $d = 0 && $h = 0 && $m = 0 ]]; then
                echo -n "$s"
        fi
        echo -n "."$ms
        echo
    fi
}
if [[ $1 ]];
    then
    convert_time "$1"
fi