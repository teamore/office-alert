#!/usr/bin/env bash

date_diff ()
{
    if [[ $1 =~ ^-?[0-9]+$ ]]; then
        now=`date +%s`
        compare=${2:-$now}
        diff=$(($compare-$1))
        if [[ $3 == "num" || $3 == "numeric" ]]; then
            echo $diff
            return 0
        fi
        if [[ $(($diff/60/60/24)) -gt 1 ]]; then
            echo $(($diff/60/60/24))" days"
        elif [[ $(($diff/60/60/24)) -gt 0 ]]; then
            echo $(($diff/60/60/24))" day"
        elif [[ $(($diff/60/60)) -gt 1 ]]; then
            echo $(($diff/60/60))" hours"
        elif [[ $(($diff/60/60)) -gt 0 ]]; then
            echo $(($diff/60/60))" hour"
        elif [[ $(($diff/60)) -gt 1 ]]; then
            echo $(($diff/60))" minutes"
        elif [[ $(($diff/60)) -gt 0 ]]; then
            echo $(($diff/60))" minutes"
        elif [[ $(($diff)) -gt 1 ]]; then
            echo $diff" seconds"
        else
            echo "1 second"
        fi
        return 0
    fi

    drex='[0-9]{4}\-[0-9]{2}\-[0-9]{2}'

    if [[ $1 =~ $drex ]]; then

        now=`date +%Y-%m-%d\ %H:%M:%S`

        compare=${2:-$now}
        lastPlay=$1

        diff=`echo $(( ( $(date -ud "$compare" +'%s') - $(date -ud "$lastPlay" +'%s') ) ))`
        if [[ $3 == "num" || $3 == "numeric" ]]; then
            echo $diff
            return 0
        fi
        diff=`date -u -d @${diff} +"%Y,%m,%V,%d,%H,%M,%S"`

        IFS=, read years months weeks days hours minutes seconds <<< $diff

        months=$((10#$months))
        hours=$((10#$hours))
        minutes=$((10#$minutes))
        seconds=$((10#$seconds))

        if [ $years -gt 1970 ]
        then
            diff="$((years-1970)) year"
        elif [ $months -gt 1 ];
            then diff="$((months-1)) month"
        elif [ $weeks -gt 1 ];
            then diff="$((weeks-1)) week"
        elif [ $days -gt 1 ];
            then diff="$((days-1)) day"
        elif [ $hours -gt 0 ];
            then diff="$((hours)) hour"
        elif [ $minutes -gt 0 ];
            then diff="$((minutes)) minute"
        elif [ $seconds -gt 0 ];
            then diff="$((seconds)) second"
        fi
        unit=`echo "$diff" | grep -o "[0-9]*"`
        if [ $unit -gt 1 ]
        then
            diff=$diff"s"
        fi
        echo $diff
        return 0
    fi
}
if [[ $1 ]];
    then
    date_diff "$1" "$2" "$3"
fi