#!/usr/bin/env bash

# GET CURRENT NETWORK IP ADDRESSES
ifconfig | grep -o "inet \([^ ]*\)[ ]*netmask \([^ ]*\)[ ]*broadcast \([^ ]*\)" | cut -d' ' -f2,5,8 | tr ' ' '\t'
