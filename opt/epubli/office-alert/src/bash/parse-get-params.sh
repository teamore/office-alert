#!/usr/bin/env bash

# initialization
OIFS="$IFS"
IFS="${IFS}&"
if [[ $QUERY_STRING ]]; then
    set $QUERY_STRING
fi
ARGS="$*"
IFS="$OIFS"